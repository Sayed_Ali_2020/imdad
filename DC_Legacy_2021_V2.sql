drop table MS_LGCY_DC_DOS; commit;
----- item group code risk
create table MS_LGCY_DC_DOS as

WITH
  imf AS
  (
    SELECT
      ait.COUNTRY_CODE,
      ait.ITEM_GROUP,
      NVL(ait.ITEM_GROUP, ait.ORPOS_ITEM_ID) AS it_GP,
      ait.GP_ACTIVE_ITEM_RMS,
      ait.ORPOS_ITEM_ID
    FROM
      NMCEDW.VW_W_ITEM_CAT_D_ITEMS ait
    WHERE
      ait.COUNTRY_CODE = 'KSA'
  )
  ,
  rdf AS
  (
    SELECT
      im.ITEM,
      p.*
    FROM
      rdfsp.ext_sp_dc_param@rmsp p,
      rdfsp.im@rmsp im
    WHERE
      p.SKU_GROUP = NVL(im.ITEM_GROUP, im.ITEM)
    AND p.SKU_GROUP LIKE 'GPC%'
    AND im.ITEM_GROUP     IS NOT NULL
    AND im.GROUP_MAIN_ITEM = 'Y'
    UNION
    SELECT
      im.ITEM,
      p.*
    FROM
      rdfsp.ext_sp_dc_param@rmsp p,
      rdfsp.im@rmsp im
    WHERE
      p.SKU_GROUP      = NVL(im.ITEM_GROUP, im.ITEM)
    AND im.ITEM_GROUP IS NULL
  )
SELECT
  imf.GP_ACTIVE_ITEM_RMS,
  rdf.COMPANY,
  rdf.DC,
  rdf.DC_MIN_DOS,
  rdf.DC_MAX_DOS,
  imf.ORPOS_ITEM_ID
FROM
  rdf
INNER JOIN imf
ON
  imf.ORPOS_ITEM_ID = rdf.ITEM
  where  imf.ORPOS_ITEM_ID= imf.GP_ACTIVE_ITEM_RMS
;

  commit;
  
  
  ---------------------
  
 drop table MS_DC_LGCY_SOH ; commit;
  create table MS_DC_LGCY_SOH as
WITH
  imf AS
  (
    SELECT
      ait.COUNTRY_CODE,
      ait.ITEM_GROUP,
      ait.GP_ACTIVE_ITEM_RMS
    FROM
      NMCEDW.VW_W_ITEM_CAT_D_ITEMS ait
    WHERE
      ait.COUNTRY_CODE = 'KSA'
  )
  (
   SELECT
  ld.LOC_CLASS_ID,
   ait.GP_ACTIVE_ITEM_RMS ITEM,
  s.LOC,
  SUM((nvl(s.STOCK_ON_HAND,0) - nvl(s.TSF_RESERVED_QTY,0) - nvl(s.NON_SELLABLE_QTY,0)))  STOCK_ON_HAND,
  AVG(s.AV_COST)              AS AV_COST,
  SUM(s.IN_TRANSIT_QTY)       AS IN_TRANSIT_QTY,
  SUM(s.NON_SELLABLE_QTY)     AS NON_SELLABLE_QTY,
  SUM(s.TSF_EXPECTED_QTY)     AS TSF_EXPECTED_QTY,
  SUM(s.TSF_RESERVED_QTY)     AS TSF_RESERVED_QTY,
  MAX(s.LAST_RECEIVED)        AS LAST_RECEIVED,
  SUM(s.QTY_RECEIVED)         AS QTY_RECEIVED,
  MAX(s.PRIMARY_SUPP)         AS PRIMARY_SUPP,
  MIN(s.TIME_ID)              AS TIME_ID,
  MIN(s.FIRST_RECEIVED)       AS FIRST_RECEIVED,
  MAX(s.LAST_UPDATE_ID)       AS LAST_UPDATE_ID,
  MAX(s.LAST_UPDATE_DATETIME) AS LAST_UPDATE_DATETIME,
  MIN(s.CREATE_DATETIME)      AS CREATE_DATETIME
 
FROM
  NMCEDW.W_ITEM_LOC_SOH_CURR_FS s
INNER JOIN nmcedw.w_location_d ld
ON
  ld.LOCATION_ID = s.LOC
INNER JOIN NMCEDW.VW_W_ITEM_CAT_D_ITEMS ait
ON
  ait.ORPOS_ITEM_ID = s.ITEM
WHERE
  ld.LOC_CLASS_ID = 5001
GROUP BY
  ld.LOC_CLASS_ID,
    s.LOC,
  ait.GP_ACTIVE_ITEM_RMS
  );
  commit;
  -----------------------------------------------------------
  drop table MS_LGCY_DC_DEMAND; commit;
  Create table MS_LGCY_DC_DEMAND as
WITH
  t1 AS
  (
    SELECT
      x.DC,
      x.DC_MIN_DOS,
      x.DC_MAX_DOS,
      NVL(x.DC_MAX_DOS, 0) + 30 AS DC_MAX_DOS_BF,
      m.GP_ACTIVE_ITEM_RMS
    FROM
      MS_LGCY_DC_DOS x
    INNER JOIN NMCEDW.VW_W_ITEM_CAT_D_ITEMS m
    ON
      x.GP_ACTIVE_ITEM_RMS = m.ORPOS_ITEM_ID
  )
SELECT
  m.GP_ACTIVE_ITEM_RMS as ITEM,
  t1.DC LOCATION_ID,
  t1.DC_MIN_DOS,
  t1.DC_MAX_DOS,
  t1.DC_MAX_DOS_BF,
  t1.GP_ACTIVE_ITEM_RMS AS GP_ACTIVE_ITEM_RMS1,
  dm.DC                ,
  dm.DEMAND_7_DAYS,
  dm.DEMAND_15_DAYS,
  dm.DEMAND_30_DAYS,
  dm.DEMAND_45_DAYS,
  dm.DEMAND_60_DAYS,
  dm.DEMAND_75_DAYS,
  dm.DEMAND_90_DAYS,
  dm.DEMAND_120_DAYS,
  dm.DEMAND_150_DAYS,
  dm.DEMAND_180_DAYS,
    case 
  when nvl(t1.DC_MAX_DOS,0) <= 7 then nvl(dm.DEMAND_7_DAYS,0)/7
  when nvl(t1.DC_MAX_DOS,0) <= 15 then nvl(dm.DEMAND_15_DAYS,0)/15
  when nvl(t1.DC_MAX_DOS,0) <= 30 then nvl(dm.DEMAND_30_DAYS,0)/30
  when nvl(t1.DC_MAX_DOS,0) <= 45 then nvl(dm.DEMAND_45_DAYS,0)/45
  when nvl(t1.DC_MAX_DOS,0) <= 60 then nvl(dm.DEMAND_60_DAYS,0)/60
  when nvl(t1.DC_MAX_DOS,0) <= 75 then nvl(dm.DEMAND_75_DAYS,0)/75
  when nvl(t1.DC_MAX_DOS,0) <= 90 then nvl(dm.DEMAND_90_DAYS,0)/90
  when nvl(t1.DC_MAX_DOS,0) <= 120 then nvl(dm.DEMAND_120_DAYS,0)/120
  when nvl(t1.DC_MAX_DOS,0) <= 150 then nvl(dm.DEMAND_150_DAYS,0)/150
   else  nvl(dm.DEMAND_180_DAYS,0)/180 end AVG_DEMAND_QTY_BASE ,
   
  case 
  when nvl(t1.DC_MAX_DOS_BF,0) <= 7 then nvl(dm.DEMAND_7_DAYS,0)/7
  when nvl(t1.DC_MAX_DOS_BF,0) <= 15 then nvl(dm.DEMAND_15_DAYS,0)/15
  when nvl(t1.DC_MAX_DOS_BF,0) <= 30 then nvl(dm.DEMAND_30_DAYS,0)/30
  when nvl(t1.DC_MAX_DOS_BF,0) <= 45 then nvl(dm.DEMAND_45_DAYS,0)/45
  when nvl(t1.DC_MAX_DOS_BF,0) <= 60 then nvl(dm.DEMAND_60_DAYS,0)/60
  when nvl(t1.DC_MAX_DOS_BF,0) <= 75 then nvl(dm.DEMAND_75_DAYS,0)/75
  when nvl(t1.DC_MAX_DOS_BF,0) <= 90 then nvl(dm.DEMAND_90_DAYS,0)/90
  when nvl(t1.DC_MAX_DOS_BF,0) <= 120 then nvl(dm.DEMAND_120_DAYS,0)/120
  when nvl(t1.DC_MAX_DOS_BF,0) <= 150 then nvl(dm.DEMAND_150_DAYS,0)/150
   else  nvl(dm.DEMAND_180_DAYS,0)/180 end AVG_DEMAND_QTY
FROM
  t1
INNER JOIN NMCEDW.VW_W_ITEM_CAT_D_ITEMS m
ON
  t1.GP_ACTIVE_ITEM_RMS = m.ORPOS_ITEM_ID
INNER JOIN nmcedw.W_SP_DC_EXPECTED_DEMAND_F dm
ON
  dm.ITEM = m.ORPOS_ITEM_ID
AND dm.DC = t1.DC
 ; commit;

  -------------------------------------------------------------------
  -----------------------------------------------------

---------------------------------------------------------------------------------
------------------------------------------------------------------------------------------
drop table MS_DC_LGCY_RCV; commit;
create table MS_DC_LGCY_RCV as
WITH
  imf AS
  (
    SELECT
      ait.COUNTRY_CODE,
      ait.ITEM_GROUP,
      ait.GP_ACTIVE_ITEM_RMS
    FROM
      NMCEDW.VW_W_ITEM_CAT_D_ITEMS ait
    WHERE
      ait.COUNTRY_CODE = 'KSA'
  )

  (
   SELECT
  v.LOCATION_ID,
   ait.GP_ACTIVE_ITEM_RMS ORPOS_ITEM_ID,
  MAX(v.TIME_ID)                   AS last_TIME_ID,
  SUM(v.TRANSACTION_QUANTITY) * -1 AS TSF_TO_STORE
FROM
  nmcedw.w_inventory_f_new v
INNER JOIN nmcedw.w_location_d ld
ON
  ld.LOCATION_ID = v.LOCATION_ID
INNER JOIN NMCEDW.VW_W_ITEM_CAT_D_ITEMS ait
ON
  ait.ORPOS_ITEM_ID = v.ORPOS_ITEM_ID
WHERE
  (
    v.TIME_ID
  )
                    > to_number(TO_CHAR(SysDate - 91, 'YYYYMMDD'))
AND ld.LOC_CLASS_ID = 5001
AND v.TO_LOC_TYPE   = 'S'
GROUP BY
  v.LOCATION_ID,
    ait.GP_ACTIVE_ITEM_RMS
  )
  ; commit;
   ---------------------------------------------------------------------------------------
 drop table MS_DC_LGCY_TBL_0; commit;
 create table MS_DC_LGCY_TBL_0 as
WITH
  imf AS
  (
    SELECT
      ait.COUNTRY_CODE,
      ait.ITEM_GROUP,
      ait.GP_ACTIVE_ITEM_RMS,
      ait.ORPOS_ITEM_ID
    FROM
      NMCEDW.VW_W_ITEM_CAT_D_ITEMS ait
    WHERE
      ait.COUNTRY_CODE = 'KSA'
  )
SELECT
  ld.LOC_CLASS_ID,
  s.ITEM,
  s.LOC,
  m.ITEM_TYPE,
  m.RTV,
  CASE
    WHEN m.ITEM_TYPE IN ('Liquidate', 'Cancelled', 'Recalled', 'Delisted',
      'UnTransfer')
    THEN '1_Non_regular '
    WHEN s.LAST_RECEIVED         < SysDate - 91
    AND NVL(TSF.TSF_TO_STORE, 0) = 0
    THEN '2_Stagnant  '
    WHEN NVL(TSF.TSF_TO_STORE, 0) < 0.5     * s.STOCK_ON_HAND
    AND s.LAST_RECEIVED           < SysDate - 91
    THEN '3_Semi-Stagnant'
    WHEN s.FIRST_RECEIVED < SysDate             - 91
    AND s.STOCK_ON_HAND   > fcst.AVG_DEMAND_QTY * (NVL(fcst.DC_MAX_DOS_BF, 0))
    THEN '4_Overstock '
    ELSE 'regular'
  END AS legacy_type,
  CASE
    WHEN m.ITEM_TYPE IN ('Liquidate', 'Cancelled', 'Recalled', 'Delisted',
      'UnTransfer')
    THEN s.STOCK_ON_HAND
    WHEN s.LAST_RECEIVED         < SysDate - 91
    AND NVL(TSF.TSF_TO_STORE, 0) = 0
    THEN greatest(0, s.STOCK_ON_HAND - fcst.AVG_DEMAND_QTY_BASE * (NVL(
      fcst.DC_MAX_DOS, 0)))
    WHEN NVL(TSF.TSF_TO_STORE, 0) < 0.5     * s.STOCK_ON_HAND
    AND s.LAST_RECEIVED           < SysDate - 91
    THEN greatest(0, s.STOCK_ON_HAND        - fcst.AVG_DEMAND_QTY_BASE * (NVL(
      fcst.DC_MAX_DOS, 0)))
    WHEN s.FIRST_RECEIVED < SysDate             - 91
    AND s.STOCK_ON_HAND   > fcst.AVG_DEMAND_QTY * (NVL(fcst.DC_MAX_DOS_BF, 0))
    THEN greatest(0, s.STOCK_ON_HAND            - fcst.DC_MAX_DOS_BF * (NVL(
      fcst.DC_MAX_DOS_BF, 0)))
    ELSE 0
  END AS legacy_QTY,
  m.PRICE_LIST,
  s.STOCK_ON_HAND,
  s.STOCK_ON_HAND * s.AV_COST    AS SOH_WAC,
  s.STOCK_ON_HAND * m.PRICE_LIST AS SOH_RSP,
  s.AV_COST,
  s.IN_TRANSIT_QTY,
  s.NON_SELLABLE_QTY,
  s.TSF_EXPECTED_QTY,
  s.TSF_RESERVED_QTY,
  s.LAST_RECEIVED,
  s.QTY_RECEIVED,
  s.PRIMARY_SUPP,
  s.TIME_ID,
  s.FIRST_RECEIVED,
  s.LAST_UPDATE_ID,
  s.LAST_UPDATE_DATETIME,
  s.CREATE_DATETIME,
  TSF.TSF_TO_STORE,
  TSF.LAST_TIME_ID,
  fcst.AVG_DEMAND_QTY,
  fcst.AVG_DEMAND_QTY_BASE,
  imf.ITEM_GROUP,
  fcst.DC,
  fcst.DC_MIN_DOS,
  fcst.DC_MAX_DOS,
  m.MAIN_CATEGORY,
  m.ITEM_DESCRIPTION,
  m.DEFAULT_CLASS,
  m.PRIVATE_OR_BRAND,
  m.ITEM_DEPARTMENT,
  m.ITEM_DIVISION,
  m.SITES_CODE,
  m.VENDOR_NAME,
  m.BUSINESS_TYPE,
  m.MAIN_CLASS,
  m.SUB_CLASS
FROM
  "1484".MS_DC_LGCY_SOH s
INNER JOIN nmcedw.w_location_d ld
ON
  ld.LOCATION_ID = s.LOC
INNER JOIN nmcedw.w_item_cat_d m
ON
  m.ORPOS_ITEM_ID = s.ITEM
LEFT JOIN "1484".MS_DC_LGCY_RCV TSF
ON
  s.LOC    = TSF.LOCATION_ID
AND s.ITEM = TSF.ORPOS_ITEM_ID
LEFT JOIN "1484".MS_LGCY_DC_DEMAND fcst
ON
  s.ITEM  = fcst.ITEM
AND s.LOC = fcst.LOCATION_ID
INNER JOIN imf
ON
  imf.GP_ACTIVE_ITEM_RMS = s.ITEM
LEFT JOIN "1484".MS_LGCY_DC_DOS dos
ON
  dos.DC                   = s.LOC
AND dos.GP_ACTIVE_ITEM_RMS = imf.ORPOS_ITEM_ID
WHERE
  ld.LOC_CLASS_ID = 5001
AND
  (
    s.STOCK_ON_HAND - s.NON_SELLABLE_QTY - s.TSF_RESERVED_QTY
  )
  > 0
  ; commit